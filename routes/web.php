<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',['as'=>'home','uses'=>'Dashboard\MainDashboardController@home']);
// Password Reset Routes...
Route::post('password/email', ['as' => 'password.email', 'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail']);
Route::get('password/reset', ['as' => 'password.request', 'uses' => 'Auth\ResetPasswordController@showLinkRequestForm']);
Route::post('password/reset', ['as' => '', 'uses' => 'Auth\ResetPasswordController@reset']);
Route::get('password/reset/{token}', ['as' => 'password.reset', 'uses' => 'Auth\ResetPasswordController@showResetForm']);
// Guest Routes
Route::group(['namespace' => 'Auth','middleware' => ['guest']],function (){
    Route::get('login',['as'=>'login','uses'=>'AuthController@login']);
    Route::post('login',['as'=>'web.do.login','uses'=>'AuthController@doLogin']);
    Route::get('register',['as'=>'web.register','uses'=>'AuthController@register']);
    Route::post('register',['as'=>'web.do.register','uses'=>'AuthController@doRegister']);
});

Route::get('dashboard',['as'=>'dashboard.main','uses'=>'Dashboard\MainDashboardController@dashboard']);
Route::get('schools',['as' => 'school.index','uses' => 'SchoolController@index']);
Route::get('school/{schoolId}/departments',['as' => 'department.index','uses' => 'DepartmentController@index']);
Route::get('department/{departmentId}/teachers',['as' => 'teacher.index','uses' => 'TeacherController@index']);
Route::get('administrations',['as' => 'administration.index','uses' => 'AdministrationController@index']);
Route::get('administration/{administrationId}/admins',['as' => 'admin.index','uses' => 'AdminController@index']);
Route::get('offices',['as' => 'office.index','uses' => 'OfficeController@index']);
Route::get('office/{officeId}/officers',['as' => 'officer.index','uses' => 'OfficerController@index']);
Route::get('holidays',['as' => 'holiday.index','uses' => 'HolidayController@index']);
Route::get('diary-publication-committee-members',['as' => 'publication-member.index','uses' => 'PublicationMemberController@index']);

// Auth Routes
Route::group(['middleware' => ['auth']],function (){
    Route::get('logout',['as' => 'logout','uses' => 'Auth\AuthController@logout']);
    Route::get('password-reset',['as' => 'profile.password.reset','uses' => 'Auth\AuthController@reset']);
    Route::post('password-reset',['as' => 'password.doReset','uses' => 'Auth\AuthController@doReset']);
    Route::get('profile',['as' => 'profile','uses' => 'User\ProfileController@profile']);
    Route::post('profile',['as' => 'profile.update','uses' => 'User\ProfileController@profileUpdate']);
    Route::get('profile-pic-change',['as' => 'profile.pic.change','uses' => 'User\ProfileController@profilePicChange']);
    Route::post('profile-pic-change',['as' => 'profile.pic.update','uses' => 'User\ProfileController@doProfilePicChange']);
    // laravel logs viewer
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    Route::get('report',['as' => 'report.create','uses' => 'ReportController@makeReport']);

    Route::get('school/create',['as' => 'school.create','uses' => 'SchoolController@create']);
    Route::post('school/store',['as' => 'school.store','uses' => 'SchoolController@store']);
    Route::get('school/{id}/edit',['as' => 'school.edit','uses' => 'SchoolController@edit']);
    Route::put('school/{id}/update',['as' => 'school.update','uses' => 'SchoolController@update']);
    Route::delete('school/{id}',['as' => 'school.delete','uses' => 'SchoolController@destroy']);

    Route::get('school/{schoolId}/department/create',['as' => 'department.create','uses' => 'DepartmentController@create']);
    Route::post('school/{schoolId}/department/store',['as' => 'department.store','uses' => 'DepartmentController@store']);
    Route::get('department/{id}/edit',['as' => 'department.edit','uses' => 'DepartmentController@edit']);
    Route::put('department/{id}/update',['as' => 'department.update','uses' => 'DepartmentController@update']);
    Route::delete('department/{id}',['as' => 'department.delete','uses' => 'DepartmentController@destroy']);

    Route::get('department/{departmentId}/teacher/create',['as' => 'teacher.create','uses' => 'TeacherController@create']);
    Route::post('department/{departmentId}/teacher/store',['as' => 'teacher.store','uses' => 'TeacherController@store']);
    Route::get('teacher/{id}/edit',['as' => 'teacher.edit','uses' => 'TeacherController@edit']);
    Route::put('teacher/{id}/update',['as' => 'teacher.update','uses' => 'TeacherController@update']);
    Route::delete('teacher/{id}',['as' => 'teacher.delete','uses' => 'TeacherController@destroy']);

    Route::get('designations',['as' => 'designation.index','uses' => 'DesignationController@index']);
    Route::get('designation/create',['as' => 'designation.create','uses' => 'DesignationController@create']);
    Route::post('designation/store',['as' => 'designation.store','uses' => 'DesignationController@store']);
    Route::get('designation/{id}/edit',['as' => 'designation.edit','uses' => 'DesignationController@edit']);
    Route::put('designation/{id}/update',['as' => 'designation.update','uses' => 'DesignationController@update']);
    Route::delete('designation/{id}',['as' => 'designation.delete','uses' => 'DesignationController@destroy']);

    Route::get('administration/create',['as' => 'administration.create','uses' => 'AdministrationController@create']);
    Route::post('administration/store',['as' => 'administration.store','uses' => 'AdministrationController@store']);
    Route::get('administration/{id}/edit',['as' => 'administration.edit','uses' => 'AdministrationController@edit']);
    Route::put('administration/{id}/update',['as' => 'administration.update','uses' => 'AdministrationController@update']);
    Route::delete('administration/{id}',['as' => 'administration.delete','uses' => 'AdministrationController@destroy']);

    Route::get('administration-roles',['as' => 'admin-role.index','uses' => 'AdminRoleController@index']);
    Route::get('administration-role/create',['as' => 'admin-role.create','uses' => 'AdminRoleController@create']);
    Route::post('administration-role/store',['as' => 'admin-role.store','uses' => 'AdminRoleController@store']);
    Route::get('administration-role/{id}/edit',['as' => 'admin-role.edit','uses' => 'AdminRoleController@edit']);
    Route::put('administration-role/{id}/update',['as' => 'admin-role.update','uses' => 'AdminRoleController@update']);
    Route::delete('administration-role/{id}',['as' => 'admin-role.delete','uses' => 'AdminRoleController@destroy']);

    Route::get('administration/{administrationId}/admin/create',['as' => 'admin.create','uses' => 'AdminController@create']);
    Route::post('administration/{administrationId}/admin/store',['as' => 'admin.store','uses' => 'AdminController@store']);
    Route::get('admin/{id}/edit',['as' => 'admin.edit','uses' => 'AdminController@edit']);
    Route::put('admin/{id}/update',['as' => 'admin.update','uses' => 'AdminController@update']);
    Route::delete('admin/{id}',['as' => 'admin.delete','uses' => 'AdminController@destroy']);

    Route::get('office/create',['as' => 'office.create','uses' => 'OfficeController@create']);
    Route::post('office/store',['as' => 'office.store','uses' => 'OfficeController@store']);
    Route::get('office/{id}/edit',['as' => 'office.edit','uses' => 'OfficeController@edit']);
    Route::put('office/{id}/update',['as' => 'office.update','uses' => 'OfficeController@update']);
    Route::delete('office/{id}',['as' => 'office.delete','uses' => 'OfficeController@destroy']);

    Route::get('office/{officeId}/officer/create',['as' => 'officer.create','uses' => 'OfficerController@create']);
    Route::post('office/{officeId}/officer/store',['as' => 'officer.store','uses' => 'OfficerController@store']);
    Route::get('officer/{id}/edit',['as' => 'officer.edit','uses' => 'OfficerController@edit']);
    Route::put('officer/{id}/update',['as' => 'officer.update','uses' => 'OfficerController@update']);
    Route::delete('officer/{id}',['as' => 'officer.delete','uses' => 'OfficerController@destroy']);

    Route::get('holiday/create',['as' => 'holiday.create','uses' => 'HolidayController@create']);
    Route::post('holiday/store',['as' => 'holiday.store','uses' => 'HolidayController@store']);
    Route::get('holiday/{id}/edit',['as' => 'holiday.edit','uses' => 'HolidayController@edit']);
    Route::put('holiday/{id}/update',['as' => 'holiday.update','uses' => 'HolidayController@update']);
    Route::delete('holiday/{id}',['as' => 'holiday.delete','uses' => 'HolidayController@destroy']);

    Route::get('logs',['as' => 'log.index','uses' => 'LogController@index']);
    Route::delete('log/{id}',['as' => 'log.delete','uses' => 'LogController@destroy']);

    Route::get('diary-publication-committee-member/create',['as' => 'publication-member.create','uses' => 'PublicationMemberController@create']);
    Route::post('diary-publication-committee-member/store',['as' => 'publication-member.store','uses' => 'PublicationMemberController@store']);
    Route::get('diary-publication-committee-member/{id}/edit',['as' => 'publication-member.edit','uses' => 'PublicationMemberController@edit']);
    Route::put('diary-publication-committee-member/{id}/update',['as' => 'publication-member.update','uses' => 'PublicationMemberController@update']);
    Route::delete('diary-publication-committee-member/{id}',['as' => 'publication-member.delete','uses' => 'PublicationMemberController@destroy']);


});
















/*
 * Testing phase only
 * Strictly Prohibited For Productions
 */
Route::get('test/login/{id}', 'Auth\AuthController@loginUsingId')->name('test.login');
