<?php
/**
 * Created by PhpStorm.
 * User: vivacom
 * Date: 6/1/17
 * Time: 5:54 PM
 */

namespace App\Services;


use App\Repositories\LogRepository;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Contracts\Providers\Auth;
use JWTAuth;


class LogService extends BaseService
{
    private $logRepository;
    public function __construct(LogRepository $logRepository)
    {
        $this->logRepository = $logRepository;
    }
    /**
     * return Repository instance
     *
     * @return mixed
     */
    public function baseRepository()
    {
        return $this->logRepository;
    }

    public function createLog($log){
        $user = JWTAuth::user();
        $this->create(['user_id'=>$user->id,'log'=>$log]);
    }

}