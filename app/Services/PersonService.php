<?php
/**
 * Created by PhpStorm.
 * User: vivacom
 * Date: 6/1/17
 * Time: 5:54 PM
 */

namespace App\Services;


use App\BaseSettings\Settings;
use App\Models\Person;
use Carbon\Carbon;
use Illuminate\Http\Request;


class PersonService extends BaseService
{


    public function createPerson($data)
    {
        $person = new Person;
        $person->name = $data['name'];
        $person->designation_id = $data['designation_id'];
        $person->address = $data['address'];
        $person->email = $data['email'];
        $person->personal_email = $data['personal_email'];
        $person->office_phone = $data['office_phone'];
        $person->residence_phone = $data['residence_phone'];
        $person->mobile = $data['mobile'];
        $person->fax = $data['fax'];
        $person->website = $data['website'];
        $person->joining_date = $data['joining_date'] ? Carbon::parse($data['joining_date']):Carbon::today();

        $person->save();
        return $person;
    }

    public function updatePerson($data,$personId)
    {
        $person = Person::find($personId);
        $person->name = $data['name'];
        $person->designation_id = $data['designation_id'];
        $person->address = $data['address'];
        $person->email = $data['email'];
        $person->personal_email = $data['personal_email'];
        $person->office_phone = $data['office_phone'];
        $person->residence_phone = $data['residence_phone'];
        $person->mobile = $data['mobile'];
        $person->fax = $data['fax'];
        $person->website = $data['website'];
        $person->joining_date = $data['joining_date'] ? Carbon::parse($data['joining_date']):null;
        $person->save();
        return $person;
    }


    /**
     * return Repository instance
     *
     * @return mixed
     */
    public function baseRepository()
    {
        return $this->userRepository;
    }

    public function profileUpdate(Request $request)
    {
        $data = $request->only(['name','email','phone','occupation','about']);
        $user =  $this->userRepository->updateUserInfo($data);
        return $user;
    }

    public function profilePicUpdate(Request $request)
    {
        $fileName=$this->fileUploadService->saveFile($request->file('photo'),Settings::$upload_path);
        $user =  $this->userRepository->updateProfileName($fileName);
        return $user;
    }
}