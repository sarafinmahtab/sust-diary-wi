<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\School;
use App\Services\LogService;
use Illuminate\Http\Request;
use Validator;


class DepartmentController extends Controller
{
    private $logService;

    public function __construct(LogService $logService)
    {
        $this->logService = $logService;
    }
    public function index($schoolId)
    {
        $school = School::find($schoolId);
        $departments = Department::where('school_id',$schoolId)->get();
        return view('admin.school.department.index')
            ->with('title', 'Departments of '. $school->name)
            ->with('departments', $departments)
            ->with('schoolId',$schoolId);
    }

    public function create($schoolId)
    {
        $school = School::find($schoolId);

        return view('admin.school.department.create')
            ->with('title', 'Add New Department in '.$school->name)
            ->with('schoolId',$schoolId);
    }

    public function store(Request $request,$schoolId)
    {
        $data = $request->all();

        $rules = [
            'name' => 'required',
            'code' => 'required',
            'email' => 'email'
        ];

        $validation = Validator::make($data, $rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }
        $department = new Department();
        $department->name = $data['name'];
        $department->code = $data['code'];
        $department->email = $data['email'];
        $department->website = $data['website'];
        $department->school_id = $schoolId;

        if ($department->save()) {
            $this->logService->createLog('Create:Department:ID:'.$department->id);
            return redirect()->route('department.index',$schoolId)->with('success', 'Department Added');
        } else {
            return redirect()->route('department.index',$schoolId)->with('error', 'Something went wrong');
        }
    }

    public function edit($id)
    {
        $department = Department::find($id);
        return view('admin.school.department.edit')
            ->with('title', 'Edit Department of '.$department->code)
            ->with('department',$department);
    }

    public function update(Request $request,$id)
    {
        $data = $request->all();

        $rules = [
            'name' => 'required',
            'code' => 'required',
            'email' => 'email',
        ];

        $validation = Validator::make($data, $rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }
        $department = Department::find($id);
        $department->name = $data['name'];
        $department->code = $data['code'];
        $department->email = $data['email'];
        $department->website = $data['website'];

        if ($department->save()) {
            $this->logService->createLog('Edit:Department:ID:'.$department->id);
            return redirect()->route('department.index',$department->school_id)->with('success', 'Department Successfully Updated');
        } else {
            return redirect()->back()->with('error', 'Something went wrong');
        }
    }

    public function destroy($id)
    {
        try {
            Department::destroy($id);
            $this->logService->createLog('Delete:Department:ID:'.$id);
            return redirect()->back()->with('success', 'Department Deleted Successfully.');
        } catch (\Exception $ex) {
            return redirect()->back()->with('error', 'Something went wrong.Try Again.');
        }
    }
}
