<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\Administration;
use App\Models\AdminRole;
use App\Models\Designation;
use App\Models\Officer;
use App\Models\Person;
use App\Models\Teacher;
use App\Services\LogService;
use App\Services\PersonService;
use Illuminate\Http\Request;
use Validator;


class AdminController extends Controller
{
    private $personService,$logService;

    public function __construct(PersonService $personService,LogService $logService)
    {
        $this->personService = $personService;
        $this->logService = $logService;
    }
    public function index($administrationId)
    {
//          $admins = Admin::with(['person','person.designation'])
//            ->where('administration_id',$administrationId)
//            ->orderBy('person.joining_date','asc')
//            ->get();
        $administration = Administration::find($administrationId);
        $admins = Admin::with('person','person.designation')
             ->where('administration_id',$administrationId)
             ->join('persons', 'admins.person_id', '=', 'persons.id')
             ->orderBy('persons.joining_date', 'asc')
             ->select('admins.*')
             ->get();
        return view('admin.administration.admin.index')
            ->with('title', 'Admins of '.$administration->name)
            ->with('admins', $admins)->with('administrationId',$administrationId);
    }

    public function create($administrationId)
    {
        $designations = Designation::all()->pluck('designation','id')->toArray();
        $adminRoles = AdminRole::all()->pluck('role','id')->toArray();
        $teachers = Teacher::with('person')->get()->pluck('person.name','id')->toArray();
        $officers = Officer::with('person')->get()->pluck('person.name','id')->toArray();
        $admins = Admin::with('person')->get()->pluck('person.name','id')->toArray();
        $administration = Administration::find($administrationId);
        return view('admin.administration.admin.create')
            ->with('title', 'Add New Admin in '.$administration->name)
            ->with('administrationId',$administrationId)
            ->with('adminRoles',$adminRoles)
            ->with('designations',$designations)
            ->with('teachers',$teachers)
            ->with('officers',$officers)
            ->with('admins',$admins);
    }

    public function store(Request $request,$administrationId)
    {
        $data = $request->all();

        $rules = [
            'name' => 'required',
            'designation_id' => 'required',
//            'address' => 'required',
//            'email' => 'required',
//            'personal_email' => 'required',
//            'office_phone' => 'required',
//            'residence_phone' => 'required',
//            'mobile' => 'required',
//            'fax' => 'required',
//            'website' => 'required',
        ];

        $validation = Validator::make($data, $rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }
        $person = $this->personService->createPerson($data);

        $admin = new Admin();
        $admin->person_id = $person->id;
        $admin->administration_id = $administrationId;
        $admin->admin_role_id = $data['admin_role_id'];

        if ($admin->save()) {
            $this->logService->createLog('Create:Admin:ID:'.$admin->id);
            return redirect()->route('admin.index',$administrationId)->with('success', 'Admin Added');
        } else {
            return redirect()->route('admin.index',$administrationId)->with('error', 'Something went wrong');
        }
    }

    public function edit($id)
    {
        $teachers = Teacher::with('person')->get()->pluck('person.name','id')->toArray();
        $officers = Officer::with('person')->get()->pluck('person.name','id')->toArray();
        $admins = Admin::with('person')->get()->pluck('person.name','id')->toArray();

        $designations = Designation::all()->pluck('designation','id')->toArray();
        $adminRoles = AdminRole::all()->pluck('role','id')->toArray();
        $admin = Admin::find($id);
        return view('admin.administration.admin.edit')
            ->with('title', 'Edit Admin')
            ->with('admin',$admin)
            ->with('adminRoles',$adminRoles)
            ->with('designations',$designations)
            ->with('teachers',$teachers)
            ->with('officers',$officers)
            ->with('admins',$admins);
    }

    public function update(Request $request,$id)
    {
        $data = $request->all();

        $rules = [
            'name' => 'required',
            'designation_id' => 'required',
            'admin_role_id' => 'required',
        ];

        $validation = Validator::make($data, $rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }
        $admin = Admin::find($id);
        $this->personService->updatePerson($data,$admin->person_id);
        $admin->admin_role_id = $data['admin_role_id'];
        if ($admin->save()) {
            $this->logService->createLog('Edit:Admin:ID:'.$admin->id);
            return redirect()->route('admin.index',$admin->administration_id)->with('success', 'Admin Successfully Updated');
        } else {
            return redirect()->back()->with('error', 'Something went wrong');
        }
    }

    public function destroy($id)
    {
        try {
            Admin::destroy($id);
            $this->logService->createLog('Delete:Admin:ID:'.$id);
            return redirect()->back()->with('success', 'Admin Deleted Successfully.');
        } catch (\Exception $ex) {
            return redirect()->back()->with('error', 'Something went wrong.Try Again.');
        }
    }
}
