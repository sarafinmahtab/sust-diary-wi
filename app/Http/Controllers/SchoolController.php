<?php

namespace App\Http\Controllers;

use App\Models\School;
use App\Responses\ApiResponse;
use App\Services\LogService;
use App\Transformers\Api\SchoolTransformer;
use Illuminate\Http\Request;
use Validator;

class SchoolController extends Controller
{
    private $apiResponse,$logService;

    public function __construct(ApiResponse $apiResponse,LogService $logService)
    {
        $this->apiResponse = $apiResponse;
        $this->logService = $logService;
    }

    public function allSchools(){
        $schools = School::orderBy('sequence','asc')->get();
        return $this->apiResponse->collection($schools,new SchoolTransformer());
    }

    public function index()
    {
        $schools = School::orderBy('sequence','asc')->get();
        return view('admin.school.index')
            ->with('title', 'List of all Schools')
            ->with('schools', $schools);
    }

    public function create()
    {
        return view('admin.school.create')
            ->with('title', 'Add New School');
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $rules = [
            'name' => 'required'
        ];

        $validation = Validator::make($data, $rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }
        $school = new School();
        $school->name = $data['name'];
        $school->sequence = $data['sequence'];

        if ($school->save()) {
            $this->logService->createLog('Create:School:ID:'.$school->id);
            return redirect()->route('school.index')->with('success', 'School Added');
        } else {
            return redirect()->route('school.index')->with('error', 'Something went wrong');
        }
    }

    public function edit($id)
    {
        $school = School::find($id);
        return view('admin.school.edit')
            ->with('title', 'Edit School')
            ->with('school',$school);
    }

    public function update(Request $request,$id)
    {
        $data = $request->all();

        $rules = [
            'name' => 'required',
        ];

        $validation = Validator::make($data, $rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }
        $school = School::find($id);
        $school->name = $data['name'];
        $school->sequence = $data['sequence'];

        if ($school->save()) {
            $this->logService->createLog('Edit:School:ID:'.$school->id);
            return redirect()->route('school.index')->with('success', 'School Successfully Updated');
        } else {
            return redirect()->route('school.index')->with('error', 'Something went wrong');
        }
    }

    public function destroy($id)
    {
        try {
            School::destroy($id);
            $this->logService->createLog('Delete:School:ID:'.$id);
            return redirect()->route('school.index')->with('success', 'School Deleted Successfully.');

        } catch (\Exception $ex) {
            return redirect()->route('school.index')->with('error', 'Something went wrong.Try Again.');
        }
    }
}
