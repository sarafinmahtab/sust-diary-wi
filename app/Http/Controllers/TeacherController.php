<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\Designation;
use App\Models\Teacher;
use App\Services\LogService;
use App\Services\PersonService;
use Illuminate\Http\Request;
use Validator;


class TeacherController extends Controller
{
    private $personService,$logService;

    public function __construct(PersonService $personService,LogService $logService)
    {
        $this->personService = $personService;
        $this->logService = $logService;
    }
    public function index($departmentId)
    {
//        $teachers = Teacher::with('person','person.designation')
//            ->where('department_id',$departmentId)
//            ->get();
        $department = Department::find($departmentId);
        $teachers = Teacher::with('person','person.designation')
            ->where('department_id',$departmentId)
            ->join('persons', 'teachers.person_id', '=', 'persons.id')
            ->orderBy('persons.joining_date', 'asc')
            ->select('teachers.*')
            ->get();

        return view('admin.school.department.teacher.index')
            ->with('title', 'List of Teachers in Department of '.$department->code)
            ->with('teachers', $teachers)->with('departmentId',$departmentId);
    }

    public function create($departmentId)
    {
        $department = Department::find($departmentId);

        $designations = Designation::all()->pluck('designation','id')->toArray();
        return view('admin.school.department.teacher.create')
            ->with('title', 'Add New Teacher in Department of '.$department->code)
            ->with('departmentId',$departmentId)->with('designations',$designations);
    }

    public function store(Request $request,$departmentId)
    {
        $data = $request->all();

        $rules = [
            'name' => 'required',
            'designation_id' => 'required',
//            'address' => 'required',
//            'email' => 'required',
//            'personal_email' => 'required',
//            'office_phone' => 'required',
//            'residence_phone' => 'required',
//            'mobile' => 'required',
//            'fax' => 'required',
//            'website' => 'required',
        ];

        $validation = Validator::make($data, $rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }
        $person = $this->personService->createPerson($data);

        $teacher = new Teacher();
        $teacher->person_id = $person->id;
        $teacher->department_id = $departmentId;

        if ($teacher->save()) {
            $this->logService->createLog('Create:Teacher:ID:'.$teacher->id);
            return redirect()->route('teacher.index',$departmentId)->with('success', 'Teacher Added');
        } else {
            return redirect()->route('teacher.index',$departmentId)->with('error', 'Something went wrong');
        }
    }

    public function edit($id)
    {
        $designations = Designation::all()->pluck('designation','id')->toArray();
        $teacher = Teacher::find($id);
        return view('admin.school.department.teacher.edit')
            ->with('title', 'Edit Teacher')
            ->with('teacher',$teacher)->with('designations',$designations);
    }

    public function update(Request $request,$id)
    {
        $data = $request->all();

        $rules = [
            'name' => 'required',
            'designation_id' => 'required',
        ];

        $validation = Validator::make($data, $rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }
        $teacher = Teacher::find($id);
        $this->personService->updatePerson($data,$teacher->person_id);

        if ($teacher->save()) {
            $this->logService->createLog('Edit:Teacher:ID:'.$teacher->id);
            return redirect()->route('teacher.index',$teacher->department_id)->with('success', 'Teacher Successfully Updated');
        } else {
            return redirect()->back()->with('error', 'Something went wrong');
        }
    }

    public function destroy($id)
    {
        try {
            Teacher::destroy($id);
            $this->logService->createLog('Delete:Teacher:ID:'.$id);
            return redirect()->back()->with('success', 'Teacher Deleted Successfully.');
        } catch (\Exception $ex) {
            return redirect()->back()->with('error', 'Something went wrong.Try Again.');
        }
    }
}
