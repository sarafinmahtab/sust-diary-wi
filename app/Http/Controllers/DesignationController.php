<?php

namespace App\Http\Controllers;

use App\Models\Designation;
use App\Services\LogService;
use Illuminate\Http\Request;
use Validator;


class DesignationController extends Controller
{
    private $logService;

    public function __construct(LogService $logService)
    {
        $this->logService = $logService;
    }
    public function index()
    {
        $designations = Designation::all();
        return view('admin.designation.index')
            ->with('title', 'List of all Designations')
            ->with('designations', $designations);
    }

    public function create()
    {
        return view('admin.designation.create')
            ->with('title', 'Add New Designation');
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $rules = [
            'designation' => 'required'
        ];

        $validation = Validator::make($data, $rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }
        $designation = new Designation();
        $designation->designation = $data['designation'];
        $designation->rank = $data['rank'];

        if ($designation->save()) {
            $this->logService->createLog('Create:Designation:ID:'.$designation->id);
            return redirect()->route('designation.index')->with('success', 'Designation Added');
        } else {
            return redirect()->route('designation.index')->with('error', 'Something went wrong');
        }
    }

    public function edit($id)
    {
        $designation = Designation::find($id);
        return view('admin.designation.edit')
            ->with('title', 'Edit Designation')
            ->with('designation',$designation);
    }

    public function update(Request $request,$id)
    {
        $data = $request->all();

        $rules = [
            'designation' => 'required',
        ];

        $validation = Validator::make($data, $rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }
        $designation = Designation::find($id);
        $designation->designation = $data['designation'];
        $designation->rank = $data['rank'];

        if ($designation->save()) {
            $this->logService->createLog('Edit:Designation:ID:'.$designation->id);
            return redirect()->route('designation.index')->with('success', 'Designation Successfully Updated');
        } else {
            return redirect()->route('designation.index')->with('error', 'Something went wrong');
        }
    }

    public function destroy($id)
    {
        try {
            Designation::destroy($id);
            $this->logService->createLog('Delete:Designation:ID:'.$id);
            return redirect()->route('designation.index')->with('success', 'Designation Deleted Successfully.');

        } catch (\Exception $ex) {
            return redirect()->route('designation.index')->with('error', 'Something went wrong.Try Again.');
        }
    }
}
