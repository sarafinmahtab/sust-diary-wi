<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\Officer;
use App\Models\PublicationMember;
use App\Models\Designation;
use App\Models\Person;
use App\Models\Teacher;
use App\Services\LogService;
use App\Services\PersonService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;


class PublicationMemberController extends Controller
{
    private $personService,$logService;

    public function __construct(PersonService $personService,LogService $logService)
    {
        $this->personService = $personService;
        $this->logService = $logService;
    }
    public function index()
    {
//        $publicationMembers = PublicationMember::all();
        $publicationMembers = PublicationMember::with('person','person.designation')
            ->join('persons', 'publication_members.person_id', '=', 'persons.id')
            ->orderBy('persons.joining_date', 'asc')
            ->select('publication_members.*')
            ->get();
        return view('admin.publication-member.index')
            ->with('title', 'List of Diary Publication Committee Members')
            ->with('publicationMembers', $publicationMembers);
    }

    public function create()
    {
        $designations = Designation::all()->pluck('designation','id')->toArray();
        $teachers = Teacher::with('person')->get()->pluck('person.name','id')->toArray();
        $officers = Officer::with('person')->get()->pluck('person.name','id')->toArray();
        $admins = Admin::with('person')->get()->pluck('person.name','id')->toArray();
        return view('admin.publication-member.create')
            ->with('title', 'Diary Publication Committee Member')
            ->with('admins',$admins)
            ->with('teachers',$teachers)
            ->with('officers',$officers)
            ->with('designations',$designations);
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $rules = [
            'name' => 'required',
            'designation_id' => 'required',
//            'address' => 'required',
//            'email' => 'required',
//            'personal_email' => 'required',
//            'office_phone' => 'required',
//            'residence_phone' => 'required',
//            'mobile' => 'required',
//            'fax' => 'required',
//            'website' => 'required',
        ];

        $validation = Validator::make($data, $rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }
        $person = $this->personService->createPerson($data);

        $publicationMember = new PublicationMember();
        $publicationMember->person_id = $person->id;
        $publicationMember->office_name = $request->office_name;
        $publicationMember->role = $request->role;

        if ($publicationMember->save()) {
            $this->logService->createLog('Create:PublicationMember:ID:'.$publicationMember->id);
            return redirect()->route('publication-member.index')->with('success', 'Member Added');
        } else {
            return redirect()->route('publication-member.index')->with('error', 'Something went wrong');
        }
    }

    public function edit($id)
    {
        $designations = Designation::all()->pluck('designation','id')->toArray();
        $teachers = Teacher::with('person')->get()->pluck('person.name','id')->toArray();
        $officers = Officer::with('person')->get()->pluck('person.name','id')->toArray();
        $admins = Admin::with('person')->get()->pluck('person.name','id')->toArray();
        $publicationMember = PublicationMember::find($id);
        return view('admin.publication-member.edit')
            ->with('title', 'Edit Diary Publication Committee Member')
            ->with('publicationMember',$publicationMember)
            ->with('teachers',$teachers)
            ->with('admins',$admins)
            ->with('officers',$officers)
            ->with('designations',$designations);
    }

    public function update(Request $request,$id)
    {
        $data = $request->all();

        $rules = [
            'name' => 'required',
            'designation_id' => 'required',
        ];

        $validation = Validator::make($data, $rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }
        $publicationMember = PublicationMember::find($id);
        $this->personService->updatePerson($data,$publicationMember->person_id);
        $publicationMember->office_name = $request->office_name;
        $publicationMember->role = $request->role;

        if ($publicationMember->save()) {
            $this->logService->createLog('Edit:PublicationMember:ID:'.$publicationMember->id);
            return redirect()->route('publication-member.index')->with('success', 'Member Successfully Updated');
        } else {
            return redirect()->back()->with('error', 'Something went wrong');
        }
    }

    public function destroy($id)
    {
        try {
            PublicationMember::destroy($id);
            $this->logService->createLog('Delete:PublicationMember:ID:'.$id);
            return redirect()->back()->with('success', 'Member Deleted Successfully.');
        } catch (\Exception $ex) {
            return redirect()->back()->with('error', 'Something went wrong.Try Again.');
        }
    }

    public function getPerson($id, $isTeacher){
        if($isTeacher=='Teacher'){
            $user = Teacher::find($id);
            $office = 'Dept of '.$user->department->code;
        }elseif ($isTeacher=='Admin'){
            $user  = Admin::find($id);
            $office = $user->administration->name;
        }else{
            $user = Officer::find($id);
            $office = $user->office->name;
        }
        $person = Person::find($user->person_id);

        $person->office = $office;

        $person->joining_date = Carbon::parse($person->joining_date)->toDateString();
        return $person;
    }
}
