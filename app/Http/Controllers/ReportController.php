<?php

namespace App\Http\Controllers;

use App\Models\Administration;
use App\Models\Holiday;
use App\Models\Log;
use App\Models\Office;
use App\Models\PublicationMember;
use App\Models\School;
use Carbon\Carbon;
use Illuminate\Http\Request;
use PDF;
use View;

class ReportController extends Controller
{
    public function makeReport1(){
        set_time_limit(1200);
        $schools = School::with(['departments','departments.teachers','departments.teachers.person.designation','departments.teachers.person.designation'])->get();
        $administrations = Administration::with(['admins','admins.person','admins.person.designation'])->get();
        $offices = Office::with(['officers','officers.person','officers.person.designation'])->get();
        $publicationMembers = PublicationMember::all();
        $holidays = Holiday::all();
        $pdf = PDF::loadView('admin.report.report', compact('publicationMembers','administrations','schools','holidays','offices'));
        return $pdf->download('diary-report.pdf');

    }

    public function makeReport(){
        set_time_limit(1200);
        $schools = School::with(['departments','departments.teachers','departments.teachers.person.designation','departments.teachers.person.designation'])->get();
        $administrations = Administration::with(['admins','admins.person','admins.person.designation'])->get();
        $offices = Office::with(['officers','officers.person','officers.person.designation'])->get();
        $publicationMembers = PublicationMember::with(['person','person.designation'])->get();
        $holidays = Holiday::all();

        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();

        $section->getStyle()->setBreakType('continuous');
        $header = $section->addHeader();
        $header->headerTop(10);

//        $section->addImage(base_url('images/qoutlogo.jpg'), array('align'=>'center' ,'topMargin' => -5));

        $section->addTextBreak(-5);
        $center = $phpWord->addParagraphStyle('p2Style', array('align'=>'center','marginTop' => 1));
        $section->addText('SUST DIARY REPORT',array('bold' => true,'underline'=>'single','name'=>'TIMOTHYfont','size' => 16),$center);
        $section->addTextBreak(2);

//        $section->addText('Tel:    00971-55-25553443 Fax: 00971-55- 2553443',array('name'=>'Times New Roman','size' => 13),$center);
//        $section->addTextBreak(-.5);
        $section->addText('DIARY PUBLICATION COMMITTEE',array('bold' => true,'underline'=>'single','name'=>'Times New Roman','size' => 14),$center);
        $section->addTextBreak(-.5);
        $tableStyle = array('borderSize' => 1, 'borderColor' => '999999', 'afterSpacing' => 0, 'Spacing'=> 0, 'cellMargin'=>0  );
        $styleCell = array('borderTopSize'=>1 ,'borderTopColor' =>'black','borderLeftSize'=>1,'borderLeftColor' =>'black','borderRightSize'=>1,'borderRightColor'=>'black','borderBottomSize' =>1,'borderBottomColor'=>'black' );
        $fontStyle = array('italic'=> true, 'size'=>11, 'name'=>'Times New Roman','afterSpacing' => 0, 'Spacing'=> 0, 'cellMargin'=>0 );
        $TfontStyle = array('bold'=>true, 'italic'=> true, 'size'=>11, 'name' => 'Times New Roman', 'afterSpacing' => 0, 'Spacing'=> 0, 'cellMargin'=>0);
        $cfontStyle = array('allCaps'=>true,'italic'=> true, 'size'=>11, 'name' => 'Times New Roman','afterSpacing' => 0, 'Spacing'=> 0, 'cellMargin'=>0);
        $noSpace = array('textBottomSpacing' => -1);
        $table = $section->addTable();
        $table->addRow();
//        $countrystate = $news['CompanyDetails']['Country'].' - '.$news['CompanyDetails']['State'];
        $table->addCell(500,[])->addText('ID',$TfontStyle);
        $table->addCell(1400,[])->addText('Name',$TfontStyle);
        $table->addCell(1000,[])->addText('Role',$TfontStyle);
        $table->addCell(1400,[])->addText('Designation',$TfontStyle);
        $table->addCell(1300,[])->addText('Office/Dept',$TfontStyle);
        $table->addCell(1200,[])->addText('Address',$TfontStyle);
        $table->addCell(1300,[])->addText('Email',$TfontStyle);
        $table->addCell(1300,[])->addText('Mobile',$TfontStyle);
        foreach ($publicationMembers as $publicationMember){
            $table->addRow();
            $table->addCell(500,[])->addText($publicationMember->id,$fontStyle);
            $table->addCell(1400,[])->addText(htmlspecialchars($publicationMember->person->name,ENT_XML1, 'UTF-8'),$fontStyle);
            $table->addCell(1000,[])->addText(htmlspecialchars($publicationMember->role,ENT_XML1, 'UTF-8'),$fontStyle);
            $table->addCell(1400,[])->addText(htmlspecialchars($publicationMember->person->designation->designation,ENT_XML1, 'UTF-8'),$fontStyle);
            $table->addCell(1300,[])->addText(htmlspecialchars($publicationMember->office_name,ENT_XML1, 'UTF-8'),$fontStyle);
            $table->addCell(1200,[])->addText(htmlspecialchars($publicationMember->person->address,ENT_XML1, 'UTF-8'),$fontStyle);
            $table->addCell(1300,[])->addText(htmlspecialchars($publicationMember->person->email,ENT_XML1, 'UTF-8'),$fontStyle);
            $table->addCell(1300,[])->addText(htmlspecialchars($publicationMember->person->mobile,ENT_XML1, 'UTF-8'),$fontStyle);
        }
        $section->addTextBreak(3);
        if(count($administrations)){
            $section->addText('ADMINISTRATIONS',array('bold' => true,'underline'=>'single','name'=>'Times New Roman','size' => 14),$center);
            $section->addTextBreak(.5);
            foreach ($administrations as $administration){
                $section->addText(htmlspecialchars($administration->name,ENT_XML1, 'UTF-8'),array('bold' => true,'underline'=>'single','name'=>'Times New Roman','size' => 14),$center);
                $section->addTextBreak(-.5);
                if(count($administration->admins)) {
                    $admins = $administration->admins
                        ->sortBy('person.joining_date')
                        ->sortBy('person.designation.rank');
                    $admins = $admins->values()->all();
                    $table = $section->addTable();
                    $table->addRow();
                    $table->addCell(600, [])->addText('ID', $TfontStyle);
                    $table->addCell(1700, [])->addText('Name', $TfontStyle);
                    $table->addCell(1100, [])->addText('Role', $TfontStyle);
                    $table->addCell(1500, [])->addText('Designation', $TfontStyle);
                    $table->addCell(1500, [])->addText('Address', $TfontStyle);
                    $table->addCell(1500, [])->addText('Email', $TfontStyle);
                    $table->addCell(1500, [])->addText('Mobile', $TfontStyle);
                    foreach ($admins as $admin) {
                        $table->addRow();
                        $table->addCell(600, [])->addText($admin->id, $fontStyle);
                        $table->addCell(1700, [])->addText(htmlspecialchars($admin->person->name, ENT_XML1, 'UTF-8'), $fontStyle);
                        $table->addCell(1100, [])->addText(htmlspecialchars($admin->adminRole->role, ENT_XML1, 'UTF-8'), $fontStyle);
                        $table->addCell(1500, [])->addText(htmlspecialchars($admin->person->designation->designation, ENT_XML1, 'UTF-8'), $fontStyle);
                        $table->addCell(1500, [])->addText(htmlspecialchars($admin->person->address, ENT_XML1, 'UTF-8'), $fontStyle);
                        $table->addCell(1500, [])->addText(htmlspecialchars($admin->person->email, ENT_XML1, 'UTF-8'), $fontStyle);
                        $table->addCell(1500, [])->addText(htmlspecialchars($admin->person->mobile, ENT_XML1, 'UTF-8'), $fontStyle);
                    }
                }
                $section->addTextBreak(1);
            }

        }

        $section->addTextBreak(3);
        if(count($schools)){
            $section->addText('SCHOOLS',array('bold' => true,'underline'=>'single','name'=>'Times New Roman','size' => 14),$center);
            $section->addTextBreak(.5);
            foreach ($schools as $school){
                $section->addText(htmlspecialchars($school->name,ENT_XML1, 'UTF-8'),array('bold' => true,'underline'=>'single','name'=>'Times New Roman','size' => 13),$center);
                $section->addTextBreak(-.5);
                if(count($school->departments)) {
                    foreach ($school->departments as $department){
                        $section->addText(htmlspecialchars($department->name,ENT_XML1, 'UTF-8'),array('bold' => true,'underline'=>'single','name'=>'Times New Roman','size' => 12),$center);
                        $section->addTextBreak(-.5);
                        $webString = '';
                        if($department->website){
                            $webString = 'Website: '.$department->website.' ';
                        }
                        if($department->email){
                            $webString.= 'Email: '.$department->email;
                        }
                        $section->addText(htmlspecialchars($webString, ENT_XML1, 'UTF-8') ,array('name'=>'Times New Roman','size' => 12),$center);
                        if(count($department->teachers)){
                            $teachers = $department->teachers
                                ->sortBy('person.joining_date')
                                ->sortBy('person.designation.rank');
                            $teachers = $teachers->values()->all();
                            $table = $section->addTable();
                            $table->addRow();
                            $table->addCell(600, [])->addText('ID', $TfontStyle);
                            $table->addCell(2000, [])->addText('Name', $TfontStyle);
                            $table->addCell(1700, [])->addText('Designation', $TfontStyle);
                            $table->addCell(1700, [])->addText('Address', $TfontStyle);
                            $table->addCell(1700, [])->addText('Email', $TfontStyle);
                            $table->addCell(1600, [])->addText('Mobile', $TfontStyle);
                            foreach ($teachers as $teacher) {
                                $table->addRow();
                                $table->addCell(600, [])->addText($teacher->id, $fontStyle);
                                $table->addCell(2000, [])->addText(htmlspecialchars($teacher->person->name, ENT_XML1, 'UTF-8'), $fontStyle);
                                $table->addCell(1700, [])->addText(htmlspecialchars($teacher->person->designation->designation, ENT_XML1, 'UTF-8'), $fontStyle);
                                $table->addCell(1700, [])->addText(htmlspecialchars($teacher->person->address, ENT_XML1, 'UTF-8'), $fontStyle);
                                $table->addCell(1700, [])->addText(htmlspecialchars($teacher->person->email, ENT_XML1, 'UTF-8'), $fontStyle);
                                $table->addCell(1600, [])->addText(htmlspecialchars($teacher->person->mobile, ENT_XML1, 'UTF-8'), $fontStyle);
                            }
                            $section->addTextBreak(1);
                        }

                    }

                }
                $section->addTextBreak(1);
            }

        }
        $section->addTextBreak(2);
        if(count($offices)) {
            $section->addText(htmlspecialchars('Offices',ENT_XML1, 'UTF-8'),array('bold' => true,'underline'=>'single','name'=>'Times New Roman','size' => 14),$center);
            $section->addTextBreak(.5);
            foreach ($offices as $office){
                $section->addText(htmlspecialchars($office->name,ENT_XML1, 'UTF-8'),array('bold' => true,'underline'=>'single','name'=>'Times New Roman','size' => 12),$center);
                $section->addTextBreak(-.5);

                if(count($office->officers)){
                    $officers = $office->officers
                        ->sortBy('person.joining_date')
                        ->sortBy('person.designation.rank');
                    $officers = $officers->values()->all();
                    $table = $section->addTable();
                    $table->addRow();
                    $table->addCell(600, [])->addText('ID', $TfontStyle);
                    $table->addCell(2000, [])->addText('Name', $TfontStyle);
                    $table->addCell(1700, [])->addText('Designation', $TfontStyle);
                    $table->addCell(1700, [])->addText('Address', $TfontStyle);
                    $table->addCell(1700, [])->addText('Email', $TfontStyle);
                    $table->addCell(1600, [])->addText('Mobile', $TfontStyle);
                    foreach ($officers as $officer) {
                        $table->addRow();
                        $table->addCell(600, [])->addText($officer->id, $fontStyle);
                        $table->addCell(2000, [])->addText(htmlspecialchars($officer->person->name, ENT_XML1, 'UTF-8'), $fontStyle);
                        $table->addCell(1700, [])->addText(htmlspecialchars($officer->person->designation->designation, ENT_XML1, 'UTF-8'), $fontStyle);
                        $table->addCell(1700, [])->addText(htmlspecialchars($officer->person->address, ENT_XML1, 'UTF-8'), $fontStyle);
                        $table->addCell(1700, [])->addText(htmlspecialchars($officer->person->email, ENT_XML1, 'UTF-8'), $fontStyle);
                        $table->addCell(1600, [])->addText(htmlspecialchars($officer->person->mobile, ENT_XML1, 'UTF-8'), $fontStyle);
                    }
                    $section->addTextBreak(1);
                }

            }

        }
        $section->addTextBreak(2);
        if(count($holidays)){
            $section->addText('HOLIDAYS',array('bold' => true,'underline'=>'single','name'=>'Times New Roman','size' => 12),$center);
            $section->addTextBreak(-.5);

            $table = $section->addTable();
            $table->addRow();
            $table->addCell(600, [])->addText('ID', $TfontStyle);
            $table->addCell(2000, [])->addText('Holiday', $TfontStyle);
            $table->addCell(1700, [])->addText('Days', $TfontStyle);
            $table->addCell(1700, [])->addText('Start Date', $TfontStyle);
            $table->addCell(1700, [])->addText('End Date', $TfontStyle);
            $table->addCell(1600, [])->addText('Depend On Moon', $TfontStyle);
            foreach ($holidays as $holiday) {
                $table->addRow();
                $table->addCell(600, [])->addText($holiday->id, $fontStyle);
                $table->addCell(2000, [])->addText(htmlspecialchars($holiday->holiday, ENT_XML1, 'UTF-8'), $fontStyle);
                $table->addCell(1700, [])->addText($holiday->days, $fontStyle);
                $table->addCell(1700, [])->addText($holiday->start_date? Carbon::parse($holiday->start_date)->toDateString():null, $fontStyle);
                $table->addCell(1700, [])->addText($holiday->end_date? Carbon::parse($holiday->end_date)->toDateString():null, $fontStyle);
                if($holiday->depend_on_moon==1)
                    $table->addCell(1600, [])->addText('YES', $fontStyle);
                else
                    $table->addCell(1600, [])->addText('NO', $fontStyle);
            }
            $section->addTextBreak(1);


        }
        $section->addTextBreak(2);

        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('SUST_DIARY_REPORT.docx');
        return response()->download(public_path('SUST_DIARY_REPORT.docx'));

    }

}
