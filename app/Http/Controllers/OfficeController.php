<?php

namespace App\Http\Controllers;

use App\Models\Office;
use App\Services\LogService;
use App\Transformers\Api\OfficeTransformer;
use Illuminate\Http\Request;
use Validator;
use App\Responses\ApiResponse;

class OfficeController extends Controller
{
    private $apiResponse,$logService;

    public function __construct(ApiResponse $apiResponse,LogService $logService)
    {
        $this->apiResponse = $apiResponse;
        $this->logService = $logService;
    }

    public function allOffices(){
        $offices = Office::orderBy('sequence','asc')->get();
        return $this->apiResponse->collection($offices,new OfficeTransformer());
    }

    public function index()
    {
        $offices = Office::orderBy('sequence','asc')->get();
        return view('admin.office.index')
            ->with('title', 'List of all Offices')
            ->with('offices', $offices);
    }

    public function create()
    {
        return view('admin.office.create')
            ->with('title', 'Add New Office');
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $rules = [
            'name' => 'required'
        ];

        $validation = Validator::make($data, $rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }
        $office = new Office();
        $office->name = $data['name'];
        $office->sequence = $data['sequence'];

        if ($office->save()) {
            $this->logService->createLog('Create:Office:ID:'.$office->id);
            return redirect()->route('office.index')->with('success', 'Office Added');
        } else {
            return redirect()->route('office.index')->with('error', 'Something went wrong');
        }
    }

    public function edit($id)
    {
        $office = Office::find($id);
        return view('admin.office.edit')
            ->with('title', 'Edit Office')
            ->with('office',$office);
    }

    public function update(Request $request,$id)
    {
        $data = $request->all();

        $rules = [
            'name' => 'required',
        ];

        $validation = Validator::make($data, $rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }
        $office = Office::find($id);
        $office->name = $data['name'];
        $office->sequence = $data['sequence'];

        if ($office->save()) {
            $this->logService->createLog('Edit:Office:ID:'.$office->id);
            return redirect()->route('office.index')->with('success', 'Office Successfully Updated');
        } else {
            return redirect()->route('office.index')->with('error', 'Something went wrong');
        }
    }

    public function destroy($id)
    {
        try {
            Office::destroy($id);
            $this->logService->createLog('Delete:Office:ID:'.$id);
            return redirect()->route('office.index')->with('success', 'Office Deleted Successfully.');

        } catch (\Exception $ex) {
            return redirect()->route('office.index')->with('error', 'Something went wrong.Try Again.');
        }
    }
}
