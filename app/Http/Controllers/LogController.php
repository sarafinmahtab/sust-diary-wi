<?php

namespace App\Http\Controllers;

use App\Models\Log;
use Illuminate\Http\Request;

class LogController extends Controller
{
    public function index()
    {
        $logs = Log::all();
        return view('admin.log.index')
            ->with('title', 'List of all Logs')
            ->with('logs', $logs);
    }

    public function destroy($id)
    {
        try {
            Log::destroy($id);

            return redirect()->route('log.index')->with('success', 'Log Deleted Successfully.');

        } catch (\Exception $ex) {
            return redirect()->route('log.index')->with('error', 'Something went wrong.Try Again.');
        }
    }
}
