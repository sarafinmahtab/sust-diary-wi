<?php

namespace App\Http\Controllers;

use App\Models\AdminRole;
use App\Services\LogService;
use Illuminate\Http\Request;
use Validator;


class AdminRoleController extends Controller
{
    private $logService;
    public function __construct(LogService $logService)
    {
        $this->logService = $logService;
    }
    public function index()
    {
        $adminRoles = AdminRole::all();
        return view('admin.administration.admin-role.index')
            ->with('title', 'List of all Administration Roles')
            ->with('adminRoles', $adminRoles);
    }

    public function create()
    {
        return view('admin.administration.admin-role.create')
            ->with('title', 'Add New Administration Role');
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $rules = [
            'role' => 'required'
        ];

        $validation = Validator::make($data, $rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }
        $adminRole = new AdminRole();
        $adminRole->role = $data['role'];
        $adminRole->note = $data['note'];

        if ($adminRole->save()) {
            $this->logService->createLog('Create:AdminRole:ID:'.$adminRole->id);
            return redirect()->route('admin-role.index')->with('success', 'Administration Role Added');
        } else {
            return redirect()->route('admin-role.index')->with('error', 'Something went wrong');
        }
    }

    public function edit($id)
    {
        $adminRole = AdminRole::find($id);
        return view('admin.administration.admin-role.edit')
            ->with('title', 'Edit Administration Role')
            ->with('adminRole',$adminRole);
    }

    public function update(Request $request,$id)
    {
        $data = $request->all();

        $rules = [
            'role' => 'required',
        ];

        $validation = Validator::make($data, $rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }
        $adminRole = AdminRole::find($id);
        $adminRole->role = $data['role'];
        $adminRole->note = $data['note'];

        if ($adminRole->save()) {
            $this->logService->createLog('Edit:AdminRole:ID:'.$adminRole->id);
            return redirect()->route('admin-role.index')->with('success', 'Administration Role Successfully Updated');
        } else {
            return redirect()->route('admin-role.index')->with('error', 'Something went wrong');
        }
    }

    public function destroy($id)
    {
        try {
            AdminRole::destroy($id);
            $this->logService->createLog('Delete:AdminRole:ID:'.$id);
            return redirect()->route('admin-role.index')->with('success', 'AdminRole Deleted Successfully.');

        } catch (\Exception $ex) {
            return redirect()->route('admin-role.index')->with('error', 'Something went wrong.Try Again.');
        }
    }
}
