<?php

namespace App\Http\Controllers;

use App\Models\Designation;
use App\Models\Office;
use App\Models\Officer;
use App\Services\LogService;
use App\Services\PersonService;
use Illuminate\Http\Request;
use Validator;

class OfficerController extends Controller
{
    private $personService,$logService;

    public function __construct(PersonService $personService,LogService $logService)
    {
        $this->personService = $personService;
        $this->logService = $logService;
    }
    public function index($officeId)
    {
        $office = Office::find($officeId);
//        $officers = Officer::where('office_id',$officeId)->get();
        $officers = Officer::with('person','person.designation')
            ->where('office_id',$officeId)
            ->join('persons', 'officers.person_id', '=', 'persons.id')
            ->orderBy('persons.joining_date', 'asc')
            ->select('officers.*')
            ->get();
        return view('admin.office.officer.index')
            ->with('title', 'Officers of '.$office->name)
            ->with('officers', $officers)->with('officeId',$officeId);
    }

    public function create($officeId)
    {
        $office = Office::find($officeId);
        $designations = Designation::all()->pluck('designation','id')->toArray();
        return view('admin.office.officer.create')
            ->with('title', 'Add New Officer in '.$office->name)
            ->with('officeId',$officeId)->with('designations',$designations);
    }

    public function store(Request $request,$officeId)
    {
        $data = $request->all();

        $rules = [
            'name' => 'required',
            'designation_id' => 'required',
//            'address' => 'required',
//            'email' => 'required',
//            'personal_email' => 'required',
//            'office_phone' => 'required',
//            'residence_phone' => 'required',
//            'mobile' => 'required',
//            'fax' => 'required',
//            'website' => 'required',
        ];

        $validation = Validator::make($data, $rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }
        $person = $this->personService->createPerson($data);

        $officer = new Officer();
        $officer->person_id = $person->id;
        $officer->office_id = $officeId;

        if ($officer->save()) {
            $this->logService->createLog('Create:Officer:ID:'.$officer->id);
            return redirect()->route('officer.index',$officeId)->with('success', 'Officer Added');
        } else {
            return redirect()->route('officer.index',$officeId)->with('error', 'Something went wrong');
        }
    }

    public function edit($id)
    {
        $designations = Designation::all()->pluck('designation','id')->toArray();
        $officer = Officer::find($id);
        return view('admin.office.officer.edit')
            ->with('title', 'Edit Officer')
            ->with('officer',$officer)->with('designations',$designations);
    }

    public function update(Request $request,$id)
    {
        $data = $request->all();

        $rules = [
            'name' => 'required',
            'designation_id' => 'required',
        ];

        $validation = Validator::make($data, $rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }
        $officer = Officer::find($id);
        $this->personService->updatePerson($data,$officer->person_id);

        if ($officer->save()) {
            $this->logService->createLog('Edit:Officer:ID:'.$officer->id);
            return redirect()->route('officer.index',$officer->office_id)->with('success', 'Officer Successfully Updated');
        } else {
            return redirect()->back()->with('error', 'Something went wrong');
        }
    }

    public function destroy($id)
    {
        try {
            Officer::destroy($id);
            $this->logService->createLog('Delete:Officer:ID:'.$id);
            return redirect()->back()->with('success', 'Officer Deleted Successfully.');
        } catch (\Exception $ex) {
            return redirect()->back()->with('error', 'Something went wrong.Try Again.');
        }
    }
}
