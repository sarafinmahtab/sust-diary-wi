<?php

namespace App\Http\Controllers;

use App\Models\Administration;
use App\Services\LogService;
use App\Transformers\Api\AdministrationTransformer;
use Illuminate\Http\Request;
use Validator;
use App\Responses\ApiResponse;


class AdministrationController extends Controller
{
    private $apiResponse,$logService;

    public function __construct(ApiResponse $apiResponse,LogService $logService)
    {
        $this->apiResponse = $apiResponse;
        $this->logService = $logService;
    }

    public function allAdministrations(){
        $administrations = Administration::orderBy('sequence','asc')->get();
        return $this->apiResponse->collection($administrations,new AdministrationTransformer());
    }

    public function index()
    {
        $administrations = Administration::orderBy('sequence','asc')->get();
        return view('admin.administration.index')
            ->with('title', 'List of all Administrations')
            ->with('administrations', $administrations);
    }

    public function create()
    {
        return view('admin.administration.create')
            ->with('title', 'Add New Administration');
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $rules = [
            'name' => 'required'
        ];

        $validation = Validator::make($data, $rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }
        $administration = new Administration();
        $administration->name = $data['name'];
        $administration->sequence = $data['sequence'];

        if ($administration->save()) {
            $this->logService->createLog('Create:Administration:ID:'.$administration->id);
            return redirect()->route('administration.index')->with('success', 'Administration Added');
        } else {
            return redirect()->route('administration.index')->with('error', 'Something went wrong');
        }
    }

    public function edit($id)
    {
        $administration = Administration::find($id);
        return view('admin.administration.edit')
            ->with('title', 'Edit Administration')
            ->with('administration',$administration);
    }

    public function update(Request $request,$id)
    {
        $data = $request->all();

        $rules = [
            'name' => 'required',
        ];

        $validation = Validator::make($data, $rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }
        $administration = Administration::find($id);
        $administration->name = $data['name'];
        $administration->sequence = $data['sequence'];

        if ($administration->save()) {
            $this->logService->createLog('Edit:Administration:ID:'.$administration->id);
            return redirect()->route('administration.index')->with('success', 'Administration Successfully Updated');
        } else {
            return redirect()->route('administration.index')->with('error', 'Something went wrong');
        }
    }

    public function destroy($id)
    {
        try {
            Administration::destroy($id);
            $this->logService->createLog('Delete:Administration:ID:'.$id);
            return redirect()->route('administration.index')->with('success', 'Administration Deleted Successfully.');

        } catch (\Exception $ex) {
            return redirect()->route('administration.index')->with('error', 'Something went wrong.Try Again.');
        }
    }
}
