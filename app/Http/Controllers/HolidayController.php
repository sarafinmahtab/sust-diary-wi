<?php

namespace App\Http\Controllers;
use App\Models\Holiday;
use App\Services\LogService;
use App\Transformers\Api\HolidayTransformer;
use Validator;
use App\Responses\ApiResponse;

use Illuminate\Http\Request;

class HolidayController extends Controller
{
    private $apiResponse,$logService;

    public function __construct(ApiResponse $apiResponse,LogService $logService)
    {
        $this->apiResponse = $apiResponse;
        $this->logService = $logService;
    }

    public function allHolidays(){
        $holidays = Holiday::all();
        return $this->apiResponse->collection($holidays,new HolidayTransformer());
    }

    public function index()
    {
        $holidays = Holiday::all();
        return view('admin.holiday.index')
            ->with('title', 'List of all Holidays')
            ->with('holidays', $holidays);
    }

    public function create()
    {
        return view('admin.holiday.create')
            ->with('title', 'Add New Holiday');
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $rules = [
            'holiday' => 'required',
            'start_date' => 'required',
            'days' => 'required',
        ];

        $validation = Validator::make($data, $rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }
        $holiday = new Holiday();
        $holiday->holiday = $data['holiday'];
        $holiday->start_date = $data['start_date'];
        $holiday->end_date = $data['end_date'];
        $holiday->days = $data['days'];
        if(isset($data['depend_on_moon']))
            $holiday->depend_on_moon = $data['depend_on_moon'];

        if ($holiday->save()) {
            $this->logService->createLog('Create:Holiday:ID:'.$holiday->id);
            return redirect()->route('holiday.index')->with('success', 'Holiday Added');
        } else {
            return redirect()->route('holiday.index')->with('error', 'Something went wrong');
        }
    }

    public function edit($id)
    {
        $holiday = Holiday::find($id);
        return view('admin.holiday.edit')
            ->with('title', 'Edit Holiday')
            ->with('holiday',$holiday);
    }

    public function update(Request $request,$id)
    {
        $data = $request->all();

        $rules = [
            'holiday' => 'required',
            'start_date' => 'required',
            'days' => 'required',
        ];

        $validation = Validator::make($data, $rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }
        $holiday = Holiday::find($id);
        $holiday->holiday = $data['holiday'];
        $holiday->start_date = $data['start_date'];
        $holiday->end_date = $data['end_date'];
        $holiday->days = $data['days'];
        if(isset($data['depend_on_moon']))
            $holiday->depend_on_moon = $data['depend_on_moon'];
        else
            $holiday->depend_on_moon = 0;

        if ($holiday->save()) {
            $this->logService->createLog('Edit:Holiday:ID:'.$holiday->id);
            return redirect()->route('holiday.index')->with('success', 'Holiday Successfully Updated');
        } else {
            return redirect()->route('holiday.index')->with('error', 'Something went wrong');
        }
    }

    public function destroy($id)
    {
        try {
            Holiday::destroy($id);
            $this->logService->createLog('Delete:Holiday:ID:'.$id);
            return redirect()->route('holiday.index')->with('success', 'Holiday Deleted Successfully.');

        } catch (\Exception $ex) {
            return redirect()->route('holiday.index')->with('error', 'Something went wrong.Try Again.');
        }
    }
}
