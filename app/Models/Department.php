<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    public function school()
    {
        return $this->belongsTo(School::class,'school_id', 'id');
    }

    public function teachers()
    {
        return $this->hasMany(Teacher::class);
    }
}
