<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Officer extends Model
{
    public function person()
    {
        return $this->belongsTo(Person::class,'person_id', 'id');
    }

    public function office()
    {
        return $this->belongsTo(Office::class,'office_id', 'id');
    }
}
