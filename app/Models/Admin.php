<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    public function person()
    {
        return $this->belongsTo(Person::class,'person_id', 'id');
    }

    public function administration()
    {
        return $this->belongsTo(Administration::class,'administration_id', 'id');
    }

    public function adminRole()
    {
        return $this->belongsTo(AdminRole::class,'admin_role_id', 'id');
    }
}
