<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Office extends Model
{
    public function officers()
    {
        return $this->hasMany(Officer::class);
    }
}
