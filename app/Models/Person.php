<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $table = 'persons';
    public function designation()
    {
        return $this->belongsTo(Designation::class,'designation_id', 'id');
    }
}
