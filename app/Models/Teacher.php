<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    public function department()
    {
        return $this->belongsTo(Department::class,'department_id', 'id');
    }

    public function person()
    {
        return $this->belongsTo(Person::class,'person_id', 'id');
    }
}
