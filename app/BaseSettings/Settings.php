<?php
namespace App\BaseSettings;
class Settings
{
    public static $company_name = 'SUST Diary';
    public static $footer_text = 'SUST';
    public static $roles = [
        'admin' => 'admin',
        'user' => 'user'
    ];

    public static $admin_role = 'admin';
    public static $client_role = 'user';
    public static $upload_path = 'uploads/';






}