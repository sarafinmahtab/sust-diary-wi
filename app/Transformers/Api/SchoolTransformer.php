<?php

namespace App\Transformers\Api;

use App\Transformers\ApiTransformerAbstract;

class SchoolTransformer extends ApiTransformerAbstract
{
    protected $availableIncludes = [
        'departments'
    ];

    /**
     * Get the fields to be transformed.
     *
     * @param $entity
     *
     * @return mixed
     */

    public function getTransformableFields($entity)
    {
        return [
            'id' => (int)$entity->id,
            'name' => $entity->name,
        ];
    }

    public function includeDepartments($entity)
    {
        $departments = $entity->departments;
        if($departments) {
            return $this->collection($departments, new DepartmentTransformer());
        }
        return null;
    }
}