<?php

namespace App\Transformers\Api;

use App\Transformers\ApiTransformerAbstract;

class OfficeTransformer extends ApiTransformerAbstract
{
    protected $availableIncludes = [
        'officers'
    ];

    /**
     * Get the fields to be transformed.
     *
     * @param $entity
     *
     * @return mixed
     */

    public function getTransformableFields($entity)
    {
        return [
            'id' => (int)$entity->id,
            'name' => $entity->name,
        ];
    }

    public function includeOfficers($entity)
    {
        $officers = $entity->officers->sortBy('person.joining_date')->sortBy('person.designation.rank');
        if($officers) {
            return $this->collection($officers, new OfficerTransformer());
        }
        return null;
    }
}