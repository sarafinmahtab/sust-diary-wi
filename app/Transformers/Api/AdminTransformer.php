<?php

namespace App\Transformers\Api;

use App\Transformers\ApiTransformerAbstract;

class AdminTransformer extends ApiTransformerAbstract
{
    /**
     * Get the fields to be transformed.
     *
     * @param $entity
     *
     * @return mixed
     */

    public function getTransformableFields($entity)
    {
        return [
            'id' => (int)$entity->id,
            'name' => $entity->person->name,
            'role' => $entity->adminRole->role,
            'designation' => $entity->person->designation->designation,
            'address' => $entity->person->address,
            'email' => $entity->person->email,
            'personalEmail' => $entity->person->personal_email,
            'officePhone' => $entity->person->office_phone,
            'residencePhone' => $entity->person->residence_phone,
            'mobile' => $entity->person->mobile,
            'fax' => $entity->person->fax,
            'website' => $entity->person->website,
            'joining_date' => $entity->person->joining_date,
        ];
    }
}