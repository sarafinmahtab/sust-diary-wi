<?php

namespace App\Transformers\Api;

use App\Transformers\ApiTransformerAbstract;

class HolidayTransformer extends ApiTransformerAbstract
{

    /**
     * Get the fields to be transformed.
     *
     * @param $entity
     *
     * @return mixed
     */

    public function getTransformableFields($entity)
    {
        return [
            'id' => (int)$entity->id,
            'holiday' => $entity->holiday,
            'startDate' => $entity->start_date,
            'endDate' => $entity->end_date,
            'days' => $entity->days,
            'dependOnMoon' => $entity->depend_on_moon
        ];
    }

}