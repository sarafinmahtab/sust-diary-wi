<?php

namespace App\Transformers\Api;

use App\Transformers\ApiTransformerAbstract;

class AdministrationTransformer extends ApiTransformerAbstract
{
    protected $availableIncludes = [
        'admins'
    ];

    /**
     * Get the fields to be transformed.
     *
     * @param $entity
     *
     * @return mixed
     */

    public function getTransformableFields($entity)
    {
        return [
            'id' => (int)$entity->id,
            'name' => $entity->name,
        ];
    }

    public function includeAdmins($entity)
    {
        $admins = $entity->admins->sortByDesc('id');
        $admins = $admins->sortBy('person.joining_date')
            ->sortBy('person.designation.rank');
        if($admins) {
            return $this->collection($admins, new AdminTransformer());
        }
        return null;
    }
}