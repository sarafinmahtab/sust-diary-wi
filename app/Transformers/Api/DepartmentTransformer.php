<?php

namespace App\Transformers\Api;

use App\Transformers\ApiTransformerAbstract;

class DepartmentTransformer extends ApiTransformerAbstract
{
    protected $availableIncludes = [
        'teachers'
    ];

    /**
     * Get the fields to be transformed.
     *
     * @param $entity
     *
     * @return mixed
     */

    public function getTransformableFields($entity)
    {
        return [
            'id' => (int)$entity->id,
            'name' => $entity->name,
            'code' => $entity->code,
            'email' => $entity->email,
            'website' => $entity->website
        ];
    }

    public function includeTeachers($entity)
    {
        $teachers = $entity->teachers
            ->sortBy('person.joining_date')
            ->sortBy('person.designation.rank');
        if($teachers) {
            return $this->collection($teachers, new TeacherTransformer());
        }
        return null;
    }
}