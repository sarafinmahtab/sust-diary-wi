<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-light " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler">
                    <span></span>
                </div>
            </li>
            <!-- END SIDEBAR TOGGLER BUTTON -->

            <li class="nav-item {!! Menu::isActiveRoute('dashboard.main') !!}">
                <a href="{!! route('dashboard.main') !!}" class="nav-link ">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                    @if (Route::is('dashboard.main'))
                        <span class="selected"></span>
                    @endif
                </a>
            </li>

            <li class="nav-item {!! Menu::areActiveRoutes(['school.index','department.index','teacher.index','school.edit','school.create']) !!}">
                <a href="{!! route('school.index') !!}" class="nav-link ">
                    <i class="icon-list"></i>
                    <span class="title">Schools</span>
                    @if (Route::is('school.index'))
                        <span class="selected"></span>
                    @endif
                </a>
            </li>

            <li class="nav-item {!! Menu::areActiveRoutes(['administration.index','admin.index','administration.edit','administration.create']) !!}">
                <a href="{!! route('administration.index') !!}" class="nav-link ">
                    <i class="icon-list"></i>
                    <span class="title">Administrations</span>
                    @if (Route::is('administration.index'))
                        <span class="selected"></span>
                    @endif
                </a>
            </li>

            <li class="nav-item {!! Menu::areActiveRoutes(['office.index','officer.index','office.edit','office.create']) !!}">
                <a href="{!! route('office.index') !!}" class="nav-link ">
                    <i class="icon-list"></i>
                    <span class="title">Offices</span>
                    @if (Route::is('office.index'))
                        <span class="selected"></span>
                    @endif
                </a>
            </li>

            <li class="nav-item {!! Menu::areActiveRoutes(['holiday.index','holiday.edit','holiday.create']) !!}">
                <a href="{!! route('holiday.index') !!}" class="nav-link ">
                    <i class="icon-list"></i>
                    <span class="title">Holidays</span>
                    @if (Route::is('holiday.index'))
                        <span class="selected"></span>
                    @endif
                </a>
            </li>
            <li class="nav-item {!! Menu::areActiveRoutes(['publication-member.index','publication-member.edit','publication-member.create']) !!}">
                <a href="{!! route('publication-member.index') !!}" class="nav-link ">
                    <i class="icon-list"></i>
                    <span class="title" style="font-size: 13px">Diary Publication Committee</span>
                    @if (Route::is('publication-member.index'))
                        <span class="selected"></span>
                    @endif
                </a>
            </li>
            @role('admin')
            <li class="nav-item {!! Menu::areActiveRoutes(['designation.index','designation.edit','designation.create']) !!}">
                <a href="{!! route('designation.index') !!}" class="nav-link ">
                    <i class="icon-list"></i>
                    <span class="title">Designations</span>
                    @if (Route::is('designation.index'))
                        <span class="selected"></span>
                    @endif
                </a>
            </li>
            <li class="nav-item {!! Menu::areActiveRoutes(['log.index']) !!}">
                <a href="{!! route('log.index') !!}" class="nav-link ">
                    <i class="icon-list"></i>
                    <span class="title">Logs</span>
                    @if (Route::is('log.index'))
                        <span class="selected"></span>
                    @endif
                </a>
            </li>
            <li class="nav-item {!! Menu::areActiveRoutes(['report.create']) !!}">
                <a href="{!! route('report.create') !!}" class="nav-link ">
                    <i class="icon-list"></i>
                    <span class="title">Report</span>
                    @if (Route::is('report.create'))
                        <span class="selected"></span>
                    @endif
                </a>
            </li>
            @endrole
        </ul>
        <!-- END SIDEBAR MENU -->
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>
<!-- END SIDEBAR -->