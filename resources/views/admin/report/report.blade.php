@extends('layouts.report-layout')
@section('content')

    <div class="row">
        @if(count($publicationMembers))
            <h4>DIARY PUBLICATION COMMITTEE</h4>
            <table>
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Role</th>
                    <th>Designation</th>
                    <th>Office/Dept</th>
                    <th>Address</th>
                    <th>Email</th>
                    <th>Mobile</th>
                </tr>
                </thead>
                <tbody>
                @foreach($publicationMembers as $publicationMember)
                    <tr>
                        <td>
                            {!! $publicationMember->id !!}
                        </td>
                        <td>
                            {!! $publicationMember->person->name !!}
                        </td>
                        <td>
                            {!! $publicationMember->role !!}
                        </td>
                        <td>
                            {!! $publicationMember->person->designation->designation !!}
                        </td>
                        <td>
                            {!! $publicationMember->office_name !!}
                        </td>
                        <td>
                            {!! $publicationMember->person->address !!}
                        </td>
                        <td>
                            {!! $publicationMember->person->email !!}
                        </td>
                        <td>
                            {!! $publicationMember->person->mobile !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endif
        @if(count($administrations))
            @foreach($administrations as $administration)
                <div class="row"><h3>{{$administration->name}}</h3></div>
                    @if(count($administration->admins))
                        <table>
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Designation</th>
                                <th>Role</th>
                                <th>Email</th>
                                <th>Mobile</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($administration->admins as $admin)
                                <tr>
                                    <td>
                                        {!! $admin->id !!}
                                    </td>
                                    <td>
                                        {!! $admin->person->name !!}
                                    </td>
                                    <td>
                                        {!! $admin->person->designation->designation !!}
                                    </td>
                                    <td>
                                        {!! $admin->adminRole->role !!}
                                    </td>
                                    <td>
                                        {!! $admin->person->email !!}
                                    </td>
                                    <td>
                                        {!! $admin->person->mobile !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @endif
            @endforeach
        @endif

        @if(count($schools))
            @foreach($schools as $school)
                <div class="row"><h3>{{$school->name}}</h3></div>
                @if(count($school->departments))
                    @foreach($school->departments as $department)
                        <div class="row"><h4>{{$department->name}}</h4></div>
                            @if(count($department->teachers))
                                <table>
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Designation</th>
                                        <th>Address</th>
                                        <th>Email</th>
                                        <th>Mobile</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($department->teachers as $teacher)
                                        <tr>
                                            <td>
                                                {!! $teacher->id !!}
                                            </td>
                                            <td>
                                                {!! $teacher->person->name !!}
                                            </td>
                                            <td>
                                                {!! $teacher->person->designation->designation !!}
                                            </td>
                                            <td>
                                                {!! $teacher->person->address !!}
                                            </td>
                                            <td>
                                                {!! $teacher->person->email !!}
                                            </td>
                                            <td>
                                                {!! $teacher->person->mobile !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @endif
                    @endforeach
                @endif
            @endforeach
        @endif
        @if(count($offices))
            @foreach($offices as $office)
                <div class="row"><h3>{{$office->name}}</h3></div>
                @if(count($office->officers))
                    <table>
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Designation</th>
                            <th>Address</th>
                            <th>Email</th>
                            <th>Mobile</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($office->officers as $officer)
                            <tr>
                                <td>
                                    {!! $officer->id !!}
                                </td>
                                <td>
                                    {!! $officer->person->name !!}
                                </td>
                                <td>
                                    {!! $officer->person->designation->designation !!}
                                </td>
                                <td>
                                    {!! $officer->person->address !!}
                                </td>
                                <td>
                                    {!! $officer->person->email !!}
                                </td>
                                <td>
                                    {!! $officer->person->mobile !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
            @endforeach
        @endif

        @if(count($holidays))
            <div class="row"><h3>HOLIDAYS {{Carbon\Carbon::now()->format('Y')}}</h3></div>

            <table>
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Holiday</th>
                    <th>Days</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Depend On Moon</th>
                </tr>
                </thead>
                <tbody>
                @foreach($holidays as $holiday)
                    <tr>
                        <td>
                            {!! $holiday->id !!}
                        </td>
                        <td>
                            {!! $holiday->holiday !!}
                        </td>

                        <td>
                            {!! $holiday->days !!}
                        </td>
                        <td>
                            {!! $holiday->start_date? Carbon\Carbon::parse($holiday->start_date)->toDateString():null !!}
                        </td>
                        <td>
                            {!! $holiday->end_date? Carbon\Carbon::parse($holiday->end_date)->toDateString():null !!}
                        </td>
                        <td>
                            @if($holiday->depend_on_moon==1)
                                Yes
                            @else
                                No
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endif
    </div>
@stop

@section('style')

@endsection








