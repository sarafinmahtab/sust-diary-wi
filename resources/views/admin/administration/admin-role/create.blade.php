@extends('layouts.app')
@section('content')
    <div class="wraper container-fluid">
        @include('admin.includes.alert')
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6">
                                <h4>{{ $title }}</h4>
                            </div>
                            <div class="col-md-6">
                                <a class="pull-right" href="{!! route('admin-role.index')!!}">
                                    <button class="btn btn-success">Administration Role List</button>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="panel-body">

                        <div class=" form">

                            {!! Form::open(array('route' => ['admin-role.store'] , 'method' => 'post', 'class' => 'cmxform form-horizontal tasi-form', 'files' => true)) !!}

                            <div class="form-group">
                                {!! Form::label('role', "Role*", array('class' => 'control-label col-lg-2')) !!}
                                <div class="col-lg-6">
                                    {!! Form::text('role', null, array('class' => 'form-control', 'placeholder' => 'Enter Role', 'required' => 'required', 'aria-required' =>'true')) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('note', "Note*", array('class' => 'control-label col-lg-2')) !!}
                                <div class="col-lg-6">
                                    {!! Form::text('note', null, array('class' => 'form-control', 'placeholder' => 'Enter Note', 'aria-required' =>'true')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    {!! Form::submit('Add Administration Role', array('class' => 'btn btn-success')) !!}
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>

                    </div>
                </div>

            </div>

        </div>
    </div>
@stop

