@extends('layouts.app')
@section('content')
    <div class="wraper container-fluid">
        @include('admin.includes.alert')
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6">
                                <h4>{{ $title }}</h4>
                            </div>
                            <div class="col-md-6">
                                <a class="pull-right" href="{!! route('officer.index',$officer->office_id)!!}">
                                    <button class="btn btn-success">Officer List</button>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="panel-body">

                        <div class=" form">

                            {!! Form::open(array('route' => ['officer.update',$officer->id] , 'method' => 'PUT', 'class' => 'cmxform form-horizontal tasi-form', 'files' => true)) !!}
                            <div class="form-group">
                                {!! Form::label('name', "Name*", array('class' => 'control-label col-lg-2')) !!}
                                <div class="col-lg-6">
                                    {!! Form::text('name', $officer->person->name, array('class' => 'form-control', 'required' => 'required', 'aria-required' =>'true')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('designation_id', "Designation*", array('class' => 'control-label col-lg-2')) !!}
                                <div class="col-lg-3">
                                    {!! Form::select('designation_id',$designations, $officer->person->designation_id, array('class' => 'form-control', 'placeholder' => 'Enter Designation', 'required' => 'required', 'aria-required' =>'true')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('address', "Address", array('class' => 'control-label col-lg-2')) !!}
                                <div class="col-lg-6">
                                    {!! Form::text('address', $officer->person->address, array('class' => 'form-control', 'placeholder' => 'Enter Address', 'aria-required' =>'true')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('email', "Email", array('class' => 'control-label col-lg-2')) !!}
                                <div class="col-lg-6">
                                    {!! Form::text('email', $officer->person->email, array('class' => 'form-control', 'placeholder' => 'Enter Email', 'aria-required' =>'true')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('personal_email', "Personal Email", array('class' => 'control-label col-lg-2')) !!}
                                <div class="col-lg-6">
                                    {!! Form::text('personal_email', $officer->person->personal_email, array('class' => 'form-control', 'placeholder' => 'Enter Personal Email', 'aria-required' =>'true')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('office_phone', "Office Phone", array('class' => 'control-label col-lg-2')) !!}
                                <div class="col-lg-6">
                                    {!! Form::text('office_phone', $officer->person->office_phone, array('class' => 'form-control', 'placeholder' => 'Enter Office Phone', 'aria-required' =>'true')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('residence_phone', "Residence Phone", array('class' => 'control-label col-lg-2')) !!}
                                <div class="col-lg-6">
                                    {!! Form::text('residence_phone', $officer->person->residence_phone, array('class' => 'form-control', 'placeholder' => 'Enter Residence Phone', 'aria-required' =>'true')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('mobile', "Mobile", array('class' => 'control-label col-lg-2')) !!}
                                <div class="col-lg-6">
                                    {!! Form::text('mobile', $officer->person->mobile, array('class' => 'form-control', 'placeholder' => 'Enter Mobile', 'aria-required' =>'true')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('fax', "Fax", array('class' => 'control-label col-lg-2')) !!}
                                <div class="col-lg-6">
                                    {!! Form::text('fax', $officer->person->fax, array('class' => 'form-control', 'placeholder' => 'Enter Fax', 'aria-required' =>'true')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('website', "Website", array('class' => 'control-label col-lg-2')) !!}
                                <div class="col-lg-6">
                                    {!! Form::text('website', $officer->person->website, array('class' => 'form-control', 'placeholder' => 'Enter Website', 'aria-required' =>'true')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('joining_date', "Joining Date", array('class' => 'control-label col-lg-2')) !!}
                                <div class="col-lg-6">
                                    {!! Form::date('joining_date', Carbon\Carbon::parse($officer->person->joining_date), array('class' => 'form-control', 'placeholder' => 'Joining Date', 'aria-required' =>'true','id'=>'joiningDate')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    {!! Form::submit('Update Officer', array('class' => 'btn btn-success')) !!}
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>

                    </div>
                </div>

            </div>

        </div>
    </div>
@stop

