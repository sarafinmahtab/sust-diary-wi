@extends('layouts.app')
@section('content')

    <div class="row">
        <div class="col-lg-12">
            @include('admin.includes.alert')

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-8">
                                    <h4>{{ $title }}</h4>
                                </div>
                                <div class="col-md-4">
                                    @role('admin')
                                    <a href="{!! route('office.create')!!}">
                                        <button class="btn btn-success">Add New Office</button>
                                    </a>
                                    @endrole
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    @if(count($offices))
                                        <table class="table table-striped table-bordered" id="dataTable">
                                            <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Name</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($offices as $office)
                                                <tr>
                                                    <td>
                                                        {!! $office->sequence !!}
                                                    </td>
                                                    <td>
                                                        {!! $office->name !!}
                                                    </td>

                                                    <td>
                                                        <a class="btn btn-info btn-xs btn-archive"
                                                           href="{!!route('officer.index',$office->id)!!}"
                                                           style="margin-right: 3px;">Officers</a>
                                                        @role('admin')
                                                        <a class="btn btn-success btn-xs btn-archive Editbtn"
                                                           href="{!!route('office.edit',$office->id)!!}"
                                                           style="margin-right: 3px;">Edit</a>
                                                        <a href="#" class="btn btn-danger btn-xs btn-archive deleteBtn"
                                                           data-toggle="modal" data-target="#deleteConfirm"
                                                           deleteId="{!! $office->id!!}">Delete</a>
                                                        @endrole
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @else
                                        No Office added yet. Be first to add one
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="deleteConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Confirmation</h4>
                </div>
                <div class="modal-body">
                    Are you sure to delete?
                </div>
                <div class="modal-footer">
                    {!! Form::open(array('route' => array('office.delete', 0), 'method'=> 'delete', 'class' => 'deleteForm')) !!}
                    <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
                    {!! Form::submit('Yes, Delete', array('class' => 'btn btn-primary')) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@stop

@section('style')

    {!! Html::style(asset('assets/datatables/jquery.dataTables.min.css')) !!}

@endsection

@section('script')
    <script type="text/javascript" src="{!! asset('assets/datatables/jquery.dataTables.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('assets/datatables/dataTables.bootstrap.js') !!}"></script>

    <!-- for Datatable -->
    <script type="text/javascript">

        $(document).ready(function () {
            $('#dataTable').dataTable({
                paging: true,
                // scrollX: true,
                "pageLength": 10,
                "columnDefs": [
                    {
                        "targets": [ 0 ],
                        "visible": false,
                        "searchable": false
                    }
                ]
            });

            $(document).on("click", ".deleteBtn", function () {
                var deleteId = $(this).attr('deleteId');
                console.log(deleteId);
                var url = "<?php echo URL::route('office.delete', false); ?>";
                $(".deleteForm").attr("action", url + '/' + deleteId);
            });

        });
    </script>


@stop







