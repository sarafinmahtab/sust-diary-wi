@extends('layouts.app')
@section('content')
    <div class="wraper container-fluid">
        @include('admin.includes.alert')
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6">
                                <h4>{{ $title }}</h4>
                            </div>
                            <div class="col-md-6">
                                <a class="pull-right" href="{!! route('office.index')!!}">
                                    <button class="btn btn-success">Office List</button>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="panel-body">

                        <div class=" form">

                            {!! Form::open(array('route' => 'holiday.store' , 'method' => 'post', 'class' => 'cmxform form-horizontal tasi-form', 'files' => true)) !!}

                            <div class="form-group">
                                {!! Form::label('holiday', "Holiday*", array('class' => 'control-label col-lg-2')) !!}
                                <div class="col-lg-6">
                                    {!! Form::text('holiday', null, array('class' => 'form-control', 'placeholder' => 'Enter Holiday', 'required' => 'required', 'aria-required' =>'true')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('start_date', "Start Date*", array('class' => 'control-label col-lg-2')) !!}
                                <div class="col-lg-3">
                                    {!! Form::date('start_date', null, array('class' => 'form-control', 'placeholder' => 'Enter Start Date', 'required' => 'required', 'aria-required' =>'true')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('end_date', "End Date", array('class' => 'control-label col-lg-2')) !!}
                                <div class="col-lg-3">
                                    {!! Form::date('end_date', null, array('class' => 'form-control', 'placeholder' => 'Enter End Date', 'aria-required' =>'true')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('days', "Days*", array('class' => 'control-label col-lg-2')) !!}
                                <div class="col-lg-6">
                                    {!! Form::number('days', null, array('class' => 'form-control', 'placeholder' => 'Enter Days Number', 'required' => 'required', 'aria-required' =>'true')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('depend_on_moon', "Is Depend on Moon?", array('class' => 'control-label col-lg-2')) !!}
                                <div class="col-lg-6">
                                    {!! Form::checkbox('depend_on_moon', '1') !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    {!! Form::submit('Add Holiday', array('class' => 'btn btn-success')) !!}
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>

                    </div>
                </div>

            </div>

        </div>
    </div>
@stop

@section('style')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet"
          type="text/css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.css">

@stop

@section('script')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

    <script>

        $(function () {
            $('#affiliateUrl').hide();

            $('#storeLinkType').change(function () {
                if ($(this).val() > 0) {
                    $('#affiliateUrl').show();
                } else {
                    $('#affiliateUrl').hide();

                }
            });
        });
    </script>
@stop
