@extends('layouts.app')
@section('content')
    <div class="wraper container-fluid">
        @include('admin.includes.alert')
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6">
                                <h4>{{ $title }}</h4>
                            </div>
                            <div class="col-md-6">
                                <a class="pull-right" href="{!! route('holiday.index',$holiday->id)!!}">
                                    <button class="btn btn-success">Holiday List</button>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="panel-body">

                        <div class=" form">

                            {!! Form::open(array('route' => ['holiday.update',$holiday->id] , 'method' => 'PUT', 'class' => 'cmxform form-horizontal tasi-form', 'files' => true)) !!}

                            <div class="form-group">
                                {!! Form::label('holiday', "Holiday*", array('class' => 'control-label col-lg-2')) !!}
                                <div class="col-lg-6">
                                    {!! Form::text('holiday', $holiday->holiday, array('class' => 'form-control', 'placeholder' => 'Enter Holiday', 'required' => 'required', 'aria-required' =>'true')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('start_date', "Start Date*", array('class' => 'control-label col-lg-2')) !!}
                                <div class="col-lg-3">
                                    {!! Form::date('start_date', Carbon\Carbon::parse($holiday->start_date), array('class' => 'form-control', 'placeholder' => 'Enter Start Date', 'required' => 'required', 'aria-required' =>'true')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('end_date', "End Date", array('class' => 'control-label col-lg-2')) !!}
                                <div class="col-lg-3">
                                    {!! Form::date('end_date', Carbon\Carbon::parse($holiday->end_date), array('class' => 'form-control', 'placeholder' => 'Enter End Date', 'aria-required' =>'true')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('days', "Days*", array('class' => 'control-label col-lg-2')) !!}
                                <div class="col-lg-6">
                                    {!! Form::number('days', $holiday->days, array('class' => 'form-control', 'placeholder' => 'Enter Days Number', 'required' => 'required', 'aria-required' =>'true')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('depend_on_moon', "Is Depend on Moon?", array('class' => 'control-label col-lg-2')) !!}
                                <div class="col-lg-6">
                                    @if($holiday->depend_on_moon==1)
                                        {!! Form::checkbox('depend_on_moon', '1',true) !!}
                                    @else
                                        {!! Form::checkbox('depend_on_moon', '1') !!}
                                    @endif

                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    {!! Form::submit('Update Office', array('class' => 'btn btn-success')) !!}
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>

                    </div>
                </div>

            </div>

        </div>
    </div>
@stop

@section('style')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet"
          type="text/css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.css">

@stop

@section('script')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
@stop
