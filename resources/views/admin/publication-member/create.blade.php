@extends('layouts.app')
@section('content')
    <div class="wraper container-fluid">
        @include('admin.includes.alert')
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6">
                                <h4>{{ $title }}</h4>
                            </div>
                            <div class="col-md-6">
                                <a class="pull-right" href="{!! route('publication-member.index')!!}">
                                    <button class="btn btn-success">Diary Publication Committee List</button>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="panel-body">

                        <div class=" form">

                            {!! Form::open(array('route' => ['publication-member.store'] , 'method' => 'post', 'class' => 'cmxform form-horizontal tasi-form', 'files' => true)) !!}

                            <div class="form-group">
                                {!! Form::label('is_teacher', "Is Teacher(You can select a teacher)", array('class' => 'control-label col-lg-2')) !!}
                                <div class="col-lg-3">
                                    {!! Form::radio('isTeacher', 'Teacher',true,array('id'=>'isTeacher')) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('is_admin', "Is Admin(You can select a admin)", array('class' => 'control-label col-lg-2')) !!}
                                <div class="col-lg-3">
                                    {!! Form::radio('isTeacher', 'Admin',false,array('id'=>'isAdmin')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('is_officer', "Is Officer(You can select a officer)", array('class' => 'control-label col-lg-2')) !!}
                                <div class="col-lg-3">
                                    {!! Form::radio('isTeacher', 'Officer',false,array('id'=>'isOfficer')) !!}
                                </div>
                            </div>

                            <div class="form-group" id="teacherSelect">
                                {!! Form::label('teacher_id', "Teacher(You can select a teacher)", array('class' => 'control-label col-lg-2')) !!}
                                <div class="col-lg-3">
                                    {!! Form::select('teacher_id',$teachers, null, array('class' => 'form-control chosen-select', 'placeholder' => 'Select Teacher', 'aria-required' =>'true')) !!}
                                </div>
                            </div>

                            <div class="form-group" id="adminSelect">
                                {!! Form::label('admin_id', "Admin(You can select a Admin)", array('class' => 'control-label col-lg-2')) !!}
                                <div class="col-lg-3">
                                    {!! Form::select('admin_id',$admins, null, array('class' => 'form-control chosen-select', 'placeholder' => 'Select Admin', 'aria-required' =>'true')) !!}
                                </div>
                            </div>

                            <div class="form-group" id="officerSelect">
                                {!! Form::label('officer_id', "Officer(You can select a officer)", array('class' => 'control-label col-lg-2')) !!}
                                <div class="col-lg-3">
                                    {!! Form::select('officer_id',$officers, null, array('class' => 'form-control chosen-select', 'placeholder' => 'Select Officer', 'aria-required' =>'true')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('name', "Name*", array('class' => 'control-label col-lg-2')) !!}
                                <div class="col-lg-6">
                                    {!! Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'Enter Name', 'required' => 'required', 'aria-required' =>'true', 'id'=>'name')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('designation_id', "Designation*", array('class' => 'control-label col-lg-2')) !!}
                                <div class="col-lg-3">
                                    {!! Form::select('designation_id',$designations, null, array('class' => 'form-control', 'placeholder' => 'Enter Designation', 'required' => 'required', 'aria-required' =>'true','id'=>'designationId')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('role', "Role", array('class' => 'control-label col-lg-2')) !!}
                                <div class="col-lg-6">
                                    {!! Form::text('role', null, array('class' => 'form-control', 'placeholder' => 'Enter Role in Publication Committee', 'aria-required' =>'true')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('office_name', "Office/Dept", array('class' => 'control-label col-lg-2')) !!}
                                <div class="col-lg-6">
                                    {!! Form::text('office_name', null, array('class' => 'form-control', 'placeholder' => 'Enter Office/Department Name', 'aria-required' =>'true','id'=>'officeName')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('address', "Address", array('class' => 'control-label col-lg-2')) !!}
                                <div class="col-lg-6">
                                    {!! Form::text('address', null, array('class' => 'form-control', 'placeholder' => 'Enter Address', 'aria-required' =>'true','id'=>'address')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('email', "Email", array('class' => 'control-label col-lg-2')) !!}
                                <div class="col-lg-6">
                                    {!! Form::text('email', null, array('class' => 'form-control', 'placeholder' => 'Enter Email', 'aria-required' =>'true','id'=>'email')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('personal_email', "Personal Email", array('class' => 'control-label col-lg-2')) !!}
                                <div class="col-lg-6">
                                    {!! Form::text('personal_email', null, array('class' => 'form-control', 'placeholder' => 'Enter Personal Email', 'aria-required' =>'true','id'=>'personalEmail')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('office_phone', "Office Phone", array('class' => 'control-label col-lg-2')) !!}
                                <div class="col-lg-6">
                                    {!! Form::text('office_phone', null, array('class' => 'form-control', 'placeholder' => 'Enter Office Phone', 'aria-required' =>'true','id'=>'officePhone')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('residence_phone', "Residence Phone", array('class' => 'control-label col-lg-2')) !!}
                                <div class="col-lg-6">
                                    {!! Form::text('residence_phone', null, array('class' => 'form-control', 'placeholder' => 'Enter Residence Phone', 'aria-required' =>'true','id'=>'residencePhone')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('mobile', "Mobile", array('class' => 'control-label col-lg-2')) !!}
                                <div class="col-lg-6">
                                    {!! Form::text('mobile', null, array('class' => 'form-control', 'placeholder' => 'Enter Mobile', 'aria-required' =>'true','id'=>'mobile')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('fax', "Fax", array('class' => 'control-label col-lg-2')) !!}
                                <div class="col-lg-6">
                                    {!! Form::text('fax', null, array('class' => 'form-control', 'placeholder' => 'Enter Fax', 'aria-required' =>'true','id'=>'fax')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('website', "Website", array('class' => 'control-label col-lg-2')) !!}
                                <div class="col-lg-6">
                                    {!! Form::text('website', null, array('class' => 'form-control', 'placeholder' => 'Enter Website', 'aria-required' =>'true','id'=>'website')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('joining_date', "Joining Date", array('class' => 'control-label col-lg-2')) !!}
                                <div class="col-lg-6">
                                    {!! Form::date('joining_date', null, array('class' => 'form-control', 'placeholder' => 'Joining Date', 'aria-required' =>'true','id'=>'joiningDate')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    {!! Form::submit('Add Member', array('class' => 'btn btn-success')) !!}
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>

                    </div>
                </div>

            </div>

        </div>
    </div>
@stop

@section('style')


@endsection

@section('script')
    <script type="text/javascript" src="{!! asset('assets/chosen/chosen.jquery.min.js') !!}"></script>
    <!-- for Datatable -->
    <script type="text/javascript">
        var isTeacher= '';
        $(document).ready(function () {
            $(".chosen-select").chosen({width: "100%"});
            if($('input:radio[name="isTeacher"]').val()=='Teacher'){
                $("#officerSelect").hide();
                $("#adminSelect").hide();
                isTeacher = 'Teacher';
            }else if($('input:radio[name="isTeacher"]').val()=='Admin'){
                $("#teacherSelect").hide();
                $("#officerSelect").hide();
                isTeacher = 'Admin';
            }else{
                $("#teacherSelect").hide();
                $("#adminSelect").hide();
                isTeacher = 'Officer';
            }
            $('input:radio[name="isTeacher"]').change(function(){
                if($(this).val() == 'Teacher'){
                    $("#teacherSelect").show();
                    $("#officerSelect").hide();
                    $("#adminSelect").hide();
                }else if($(this).val() == 'Admin'){
                    $("#teacherSelect").hide();
                    $("#officerSelect").hide();
                    $("#adminSelect").show();
                }else{
                    $("#teacherSelect").hide();
                    $("#officerSelect").show();
                    $("#adminSelect").hide();
                }
                isTeacher = $(this).val();
            });

            $('.chosen-select').on('change', function() {
                if(this.value ){
                    var url = '/api/v1/person/'+this.value+'/'+isTeacher;
                    $.getJSON(url, function(json_data){
                        $("#name").val(json_data.name);
                        $("#designationId").val(json_data.designation_id).trigger("chosen:updated");
                        $("#address").val(json_data.address);
                        $("#email").val(json_data.email);
                        $("#fax").val(json_data.fax);
                        $("#mobile").val(json_data.mobile);
                        $("#officeName").val(json_data.office);
                        $("#officePhone").val(json_data.office_phone);
                        $("#personalEmail").val(json_data.personal_email);
                        $("#residencePhone").val(json_data.residence_phone);
                        $("#website").val(json_data.website);
                        $("#joiningDate").val(json_data.joining_date);
                    });
                }
            });
        });
    </script>


@stop
