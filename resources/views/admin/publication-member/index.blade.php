@extends('layouts.app')
@section('content')

    <div class="row">
        <div class="col-lg-12">
            @include('admin.includes.alert')

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-6">
                                    <h4>{{ $title }}</h4>
                                </div>
                                <div class="col-md-6">
                                    @role('admin')
                                    <a class="pull-right" href="{!! route('publication-member.create')!!}">
                                        <button class="btn btn-success">Add New Publication Member</button>
                                    </a>
                                    @endrole
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    @if(count($publicationMembers))
                                        <table class="table table-striped table-bordered" id="dataTable">
                                            <thead>
                                            <tr>
                                                <th>Rank</th>
                                                <th>Name</th>
                                                <th>Role</th>
                                                <th>Designation</th>
                                                <th>Office/Dept</th>
                                                <th>Address</th>
                                                <th>Email</th>
                                                <th>Mobile</th>
                                                @role('admin')
                                                <th>Action</th>
                                                @endrole
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($publicationMembers as $publicationMember)
                                                <tr>
                                                    <td>
                                                        {!! $publicationMember->person->designation->rank !!}
                                                    </td>
                                                    <td>
                                                        {!! $publicationMember->person->name !!}
                                                    </td>
                                                    <td>
                                                        {!! $publicationMember->role !!}
                                                    </td>
                                                    <td>
                                                        {!! $publicationMember->person->designation->designation !!}
                                                    </td>
                                                    <td>
                                                        {!! $publicationMember->office_name !!}
                                                    </td>
                                                    <td>
                                                        {!! $publicationMember->person->address !!}
                                                    </td>
                                                    <td>
                                                        {!! $publicationMember->person->email !!}
                                                    </td>
                                                    <td>
                                                        {!! $publicationMember->person->mobile !!}
                                                    </td>
                                                    @role('admin')
                                                    <td>
                                                        <a class="btn btn-success btn-xs btn-archive Editbtn"
                                                           href="{!!route('publication-member.edit',$publicationMember->id)!!}"
                                                           style="margin-right: 3px;">Edit</a>
                                                        <a href="#" class="btn btn-danger btn-xs btn-archive deleteBtn"
                                                           data-toggle="modal" data-target="#deleteConfirm"
                                                           deleteId="{!! $publicationMember->id!!}">Delete</a>
                                                    </td>
                                                    @endrole

                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @else
                                        No Member added yet. Be first to add one
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="deleteConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Confirmation</h4>
                </div>
                <div class="modal-body">
                    Are you sure to delete?
                </div>
                <div class="modal-footer">
                    {!! Form::open(array('route' => array('publication-member.delete', 0), 'method'=> 'delete', 'class' => 'deleteForm')) !!}
                    <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
                    {!! Form::submit('Yes, Delete', array('class' => 'btn btn-primary')) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@stop

@section('style')

    {!! Html::style(asset('assets/datatables/jquery.dataTables.min.css')) !!}

@endsection

@section('script')
    <script type="text/javascript" src="{!! asset('assets/datatables/jquery.dataTables.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('assets/datatables/dataTables.bootstrap.js') !!}"></script>

    <!-- for Datatable -->
    <script type="text/javascript">

        $(document).ready(function () {
            $('#dataTable').dataTable({
                paging: true,
                // scrollX: true,
                "pageLength": 10,
                "order": [[ 0, "asc" ]],
                "columnDefs": [
                    {
                        "targets": [ 0 ],
                        "visible": false,
                        "searchable": false
                    }
                ]
            });

            $(document).on("click", ".deleteBtn", function () {
                var deleteId = $(this).attr('deleteId');
                console.log(deleteId);
                var url = "<?php echo URL::route('publication-member.delete', false); ?>";
                $(".deleteForm").attr("action", url + '/' + deleteId);
            });

        });
    </script>


@stop







